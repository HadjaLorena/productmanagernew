package university.jala.productmanagernew.services;

import jakarta.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import university.jala.productmanagernew.dtos.ProductDTO;
import university.jala.productmanagernew.models.ProductModel;
import university.jala.productmanagernew.repository.ProductRepository;

@Service
public class ProductService {

    @Autowired
    private ProductRepository repository;

    public ProductModel updateProduct(int id, ProductDTO productUpdated){
        ProductModel updateProduct = repository.findById(id).orElseThrow(()->new EntityNotFoundException("Produto não encontrado, verifique se o ID está correto: " +id));

        if(productUpdated.label() != null) updateProduct.setLabel(productUpdated.label());
        if(productUpdated.description() != null) updateProduct.setDescription(productUpdated.description());
        if(productUpdated.price() > 0) updateProduct.setPrice(productUpdated.price());
        if(productUpdated.quantity() > 0) updateProduct.setQuantity(productUpdated.quantity());

        return repository.save(updateProduct);
    }
}
