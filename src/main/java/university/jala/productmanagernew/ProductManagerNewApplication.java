package university.jala.productmanagernew;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProductManagerNewApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProductManagerNewApplication.class, args);
    }

}
