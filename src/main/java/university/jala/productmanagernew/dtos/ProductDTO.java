package university.jala.productmanagernew.dtos;

public record ProductDTO(String label, String description, float price, int quantity) {
}
