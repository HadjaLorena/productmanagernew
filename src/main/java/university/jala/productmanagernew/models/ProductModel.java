package university.jala.productmanagernew.models;

import jakarta.persistence.*;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import university.jala.productmanagernew.dtos.ProductDTO;

@Entity
@Table(name = "produtos")
@Getter
@Setter
@EqualsAndHashCode(of = "id")
@NoArgsConstructor
public class ProductModel {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int id;
    private String label;
    private String description;
    private float price;
    private int quantity;

    public ProductModel(ProductDTO productDTO){
        this.label = productDTO.label();
        this.description = productDTO.description();
        this.price = productDTO.price();
        this.quantity = productDTO.quantity();
    }
}
