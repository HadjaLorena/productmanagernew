package university.jala.productmanagernew.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import university.jala.productmanagernew.dtos.ProductDTO;
import university.jala.productmanagernew.models.ProductModel;
import university.jala.productmanagernew.repository.ProductRepository;
import university.jala.productmanagernew.services.ProductService;

import java.util.List;

@RequestMapping("/")

@RestController
public class APIController {

    @Autowired
    private ProductService productService;
    private final ProductRepository productRepository;

    @Autowired
    public APIController(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    //cria um produto no bd
    @PostMapping
    public ResponseEntity<ProductModel> createProduct(@RequestBody ProductDTO productDTO){
        ProductModel productModel1 = new ProductModel(productDTO);
        return ResponseEntity.status(HttpStatus.CREATED).body(productRepository.save(productModel1));
    }

    //exibe os produtos presentes no bd
    @GetMapping
    public ResponseEntity<List<ProductModel>> getProducts(){
        return ResponseEntity.ok(productRepository.findAll());
    }

    //deleta um produto do bd pelo id
    @DeleteMapping("/{id}")
    public ResponseEntity<ProductModel> deleteProduct(@PathVariable int id){
        productRepository.deleteById(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    //atualiza um produto no bd pelo id
    @PutMapping("/{id}")
    public ResponseEntity<ProductModel> changeProduct(@PathVariable int id, @RequestBody ProductDTO productDTO){
        var product = productService.updateProduct(id, productDTO);
        return ResponseEntity.status(HttpStatus.OK).body(product);
    }

    @GetMapping("/nome/{label}")
    public ResponseEntity<ProductModel> searchByLabel(@PathVariable String label){
        var product = productRepository.findByLabel(label).orElseThrow();
        return ResponseEntity.ok(product);
    }

    @GetMapping("/preco/{price}")
    public ResponseEntity<ProductModel> searchByPrice(@PathVariable float price){
        var product = productRepository.findByPrice(price).orElseThrow();
        return ResponseEntity.ok(product);
    }
}
