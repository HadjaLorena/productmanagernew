package university.jala.productmanagernew.repository;

import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import university.jala.productmanagernew.models.ProductModel;

import java.util.Optional;

@Repository
public interface ProductRepository extends JpaRepository<ProductModel, Integer>{
    Optional<ProductModel> findByLabel(String label);
    Optional<ProductModel> findByPrice(float price);
}
